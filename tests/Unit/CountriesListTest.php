<?php

namespace Tests\Unit;

use App\Services\CountriesLists\CountriesListFactory;
use App\Services\CountriesLists\Exceptions\FileNotSupportedException;
use Symfony\Component\HttpFoundation\File\File;
use Tests\TestCase;

class CountriesListTest extends TestCase
{
    /**
     * @var array|null
     */
    protected static $countries;

    /**
     * @var array|null
     */
    protected static $paths;

    protected function setUp(): void
    {
        parent::setUp();

        self::$countries = [
            ['country' => 'Ukraine', 'capital' => 'Kyiv'],
            ['country' => 'Germany', 'capital' => 'Berlin'],
            ['country' => 'USA', 'capital' => 'Washington'],
        ];

        self::$paths = [
            CountriesListFactory::TYPE_JSON => storage_path('app/test/countries.json'),
            CountriesListFactory::TYPE_XML  => storage_path('app/test/countries.xml'),
            CountriesListFactory::TYPE_CSV  => storage_path('app/test/countries.csv'),
        ];

        self::removeTestFiles();
    }

    protected static function removeTestFiles(): void
    {
        foreach (self::$paths as $path) {
            if (file_exists($path)) {
                unlink($path);
            }
        }
    }

    /**
     * @dataProvider extensionDataProvider
     * @covers \App\Services\CountriesLists\CountriesListFactory::createFromFile
     * @covers \App\Services\CountriesLists\CountriesList::generateFile
     * @covers  \App\Services\CountriesLists\CountriesListFactory::createFromFile
     * @covers  \App\Services\CountriesLists\CountriesList::toArray
     * @throws FileNotSupportedException
     */
    public function test_json_list_from_array($extension)
    {
        $list = CountriesListFactory::createFromArray(self::$countries, $extension);
        $filename = self::$paths[$extension];
        $list->generateFile($filename);

        $this->assertFileExists($filename);

        $list = CountriesListFactory::createFromFile(new File($filename));
        $this->assertEquals(self::$countries, $list->toArray());
    }

    public function extensionDataProvider()
    {
        return [
            [CountriesListFactory::TYPE_JSON],
            [CountriesListFactory::TYPE_CSV],
            [CountriesListFactory::TYPE_XML],
        ];
    }

    protected function tearDown(): void
    {
        self::removeTestFiles();

        parent::tearDown();
    }
}
