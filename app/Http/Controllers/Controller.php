<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenerateFileRequest;
use App\Http\Requests\ParseFileRequest;
use App\Services\CountriesLists\CountriesListFactory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('app');
    }

    public function parseFile(ParseFileRequest $request)
    {
        $countriesList = CountriesListFactory::createFromFile($request->file);
        $data = $countriesList->toArray();

        return response()->json($data);
    }

    public function generateFile(GenerateFileRequest $request)
    {
        $fileInfo = pathinfo($request->filename);
        $fileName = ($fileInfo['filename'] ?: 'countries-' . time()) . '.' . $request->extension;
        $filePath = storage_path('app/public/' . $fileName);

        $list = CountriesListFactory::createFromArray($request->countries, $request->extension);
        $list->generateFile($filePath);

        return response()->download($filePath)->deleteFileAfterSend();
    }
}
