<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenerateFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'extension'           => 'required',
            'filename'            => 'required',
            'countries'           => 'required|array',
            'countries.*.country' => 'required',
            'countries.*.capital' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'countries.*.country.required' => 'Each country must have a country name.',
            'countries.*.capital.required' => 'Each country must have a capital.',
        ];
    }
}
