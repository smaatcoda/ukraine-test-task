<?php

namespace App\Console\Commands;

use App\Services\CountriesLists\CountriesListFactory;
use App\Services\CountriesLists\Exceptions\FileNotSupportedException;
use Illuminate\Console\Command;
use Illuminate\Http\File;

class ConvertCountries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'countries:convert {--input-file=} {--output-file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converts a file to a different extension.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws FileNotSupportedException
     */
    public function handle()
    {
        if (!$this->validateOptions()) {
            return;
        }

        $inputFilePath = $this->option('input-file');
        $outputFilePath = $this->option('output-file');

        $outputPathInfo = pathinfo($outputFilePath);
        $outputExtension = $outputPathInfo['extension'] ?? CountriesListFactory::TYPE_JSON;

        $inputFile = CountriesListFactory::createFromFile(new File($inputFilePath));

        $outputFile = CountriesListFactory::createFromArray($inputFile->toArray(), $outputExtension);
        $outputFile->generateFile($outputFilePath);

        $this->info('File successfully converted and saved at ' . $outputFilePath);
    }

    /**
     * @return bool
     */
    private function validateOptions(): bool
    {
        if (!$this->hasOption('input-file') || empty($this->option('input-file'))) {
            $this->error('You must provide the --input-file argument!');

            return false;
        }

        if (!$this->hasOption('output-file') || empty($this->option('output-file'))) {
            $this->error('You must provide the --output-file argument!');

            return false;
        }

        if (!file_exists($this->option('input-file'))) {
            $this->error('File ' . $this->option('input-file') . ' does not exist!');

            return false;
        }

        return true;
    }
}
