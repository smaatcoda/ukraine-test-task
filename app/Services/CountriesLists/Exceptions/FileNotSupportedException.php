<?php


namespace App\Services\CountriesLists\Exceptions;


use Throwable;

class FileNotSupportedException extends \Exception
{
    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct('This file is not supported!', $code, $previous);
    }

}