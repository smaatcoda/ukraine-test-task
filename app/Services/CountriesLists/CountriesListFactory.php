<?php

namespace App\Services\CountriesLists;

use App\Services\CountriesLists\Exceptions\FileNotSupportedException;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Http\UploadedFile;

class CountriesListFactory
{
    public const TYPE_CSV  = 'csv';
    public const TYPE_JSON = 'json';
    public const TYPE_XML  = 'xml';

    /**
     * @var array
     */
    protected static $extensionMap = [
        self::TYPE_CSV  => CountriesCsv::class,
        self::TYPE_JSON => CountriesJson::class,
        self::TYPE_XML  => CountriesXml::class,
    ];

    /**
     * Creates a new country list from a file
     *
     * @param File $file
     * @return CountriesList
     * @throws FileNotSupportedException
     */
    public static function createFromFile(File $file): CountriesList
    {
        // Uploaded files come as tmp files and have no extension
        $fileExtension = $file instanceof UploadedFile ? $file->getClientOriginalExtension() : $file->getExtension();

        if (!isset(static::$extensionMap[$fileExtension])) {
            throw new FileNotSupportedException();
        }

        $fileClass = static::$extensionMap[$fileExtension];

        return $fileClass::createFromFile($file);
    }

    /**
     * Creates a new country list from an array
     *
     * @param array $data
     * @param $type
     * @return CountriesList
     * @throws FileNotSupportedException
     */
    public static function createFromArray(array $data, $type): CountriesList
    {
        if (!isset(static::$extensionMap[$type])) {
            throw new FileNotSupportedException();
        }

        $fileClass = static::$extensionMap[$type];

        return $fileClass::createFromArray($data);
    }


}
