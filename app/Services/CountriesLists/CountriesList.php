<?php


namespace App\Services\CountriesLists;

use Symfony\Component\HttpFoundation\File\File;

abstract class CountriesList
{
    /**
     * @var array
     */
    protected $data;

    /**
     * CountriesList constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Instantiates a new countries list from an array
     *
     * @param $data
     * @return CountriesList
     */
    public static function createFromArray($data): CountriesList
    {
        return new static($data);
    }

    /**
     * Returns countries list array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }

    public function ensureDirectory(string $filename): void
    {
        if (!file_exists(dirname($filename))) {
            if (!mkdir($concurrentDirectory = dirname($filename), 0777, true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
    }

    /**
     * Instantiates a new countries list from a file
     *
     * @param File $file
     * @return CountriesList
     */
    abstract public static function createFromFile(File $file): CountriesList;

    /**
     * Stores the countries list as a file
     *
     * @param string $filename
     * @return mixed
     */
    abstract public function generateFile(string $filename);
}
