<?php

namespace App\Services\CountriesLists;

use Symfony\Component\HttpFoundation\File\File;

class CountriesCsv extends CountriesList
{
    /**
     * @inheritDoc
     * @param File $file
     * @return CountriesList
     */
    public static function createFromFile(File $file): CountriesList
    {
        $data = self::csvToArray($file);
        // Remove the title array entry
        array_shift($data);

        return new self($data);
    }

    /**
     * @inheritDoc
     * @param string $filename
     * @return mixed|void
     */
    public function generateFile(string $filename)
    {
        $data = $this->data;
        // Insert titles row
        if (count($data)) {
            array_unshift($data, array_keys($data[0]));
        }

        $this->ensureDirectory($filename);

        $data = array_values($data);
        $file = fopen($filename, 'wb');

        foreach ($data as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }

    /**
     * Converts csv file to an array
     *
     * @param File $file
     * @return array
     */
    private static function csvToArray(File $file): array
    {
        $data = [];

        if (($resource = fopen($file->getRealPath(), 'rb')) !== false) {
            while (($line = fgetcsv($resource, 1000, ',')) !== false) {
                // Assign keys
                $data[] = [
                    'country' => $line[0],
                    'capital' => $line[1],
                ];
            }

            fclose($resource);
        }

        return $data;
    }
}
