<?php

namespace App\Services\CountriesLists;

use SimpleXMLElement;
use Symfony\Component\HttpFoundation\File\File;

class CountriesXml extends CountriesList
{
    /**
     * @inheritDoc
     * @param File $file
     * @return CountriesList
     */
    public static function createFromFile(File $file): CountriesList
    {
        $contents = file_get_contents($file->getRealPath());
        $data = self::xmlToArray($contents);
        // Unpack data from the row that came from xml root element
        $data = $data['element'] ?? [];

        return new self($data);
    }

    /**
     * @inheritDoc
     * @param string $filename
     * @return mixed|void
     */
    public function generateFile(string $filename)
    {
        $data = $this->data;
        $xml = self::arrayToXml($data);
        $this->ensureDirectory($filename);
        $file = fopen($filename, 'wb');
        fwrite($file, $xml);
        fclose($file);
    }

    /**
     * Converts an xml string to an array
     *
     * @param string $contents
     * @return mixed
     */
    private static function xmlToArray(string $contents): array
    {
        $xml = new SimpleXMLElement($contents);
        $json = json_encode($xml);

        return json_decode($json, true);
    }

    /**
     * Converts an array xml
     *
     * @param $array
     * @param null $rootElement
     * @param null $xml
     * @return mixed
     */
    private static function arrayToXml($array, $rootElement = null, $xml = null)
    {
        // If there is no Root Element then insert root
        if ($xml === null) {
            $xml = new SimpleXMLElement($rootElement ?? '<?xml version="1.0" encoding="UTF-8"?><root/>');
        }

        // Visit all key value pair
        foreach ($array as $key => $value) {
            $key = is_numeric($key) ? 'element' : $key;
            // If there is nested array then
            if (is_array($value)) {
                // Call function for nested array
                self::arrayToXml($value, $key, $xml->addChild($key));
            } else {
                // Simply add child element.
                $xml->addChild($key, $value);
            }
        }

        return $xml->asXML();
    }
}
