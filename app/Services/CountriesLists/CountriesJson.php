<?php

namespace App\Services\CountriesLists;

use Symfony\Component\HttpFoundation\File\File;

class CountriesJson extends CountriesList
{
    /**
     * @inheritDoc
     * @param File $file
     * @return CountriesList
     */
    public static function createFromFile(File $file): CountriesList
    {
        $json = file_get_contents($file->getRealPath());
        $data = json_decode($json, true);

        return new self($data);
    }

    /**
     * @inheritDoc
     * @param string $filename
     * @return mixed|void
     */
    public function generateFile(string $filename)
    {
        $this->ensureDirectory($filename);
        $file = fopen($filename, 'wb');
        fwrite($file, json_encode($this->data));
        fclose($file);
    }
}
